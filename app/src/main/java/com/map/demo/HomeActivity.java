package com.map.demo;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class HomeActivity extends FragmentActivity implements OnMapReadyCallback {

    private final String[] permissions = {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION};

    private final int LOCATION_PERMISSIONS_REQUEST = 101;
    private final int LOCATION_SERVICE_SETTING_REQUEST = 102;

    private GoogleMap mGoogleMap;
    private Location mLocation;

    private LatLng pickUpLocation, dropLocation;

    private Polyline mPolyLine;
    private Marker pickupMarker;
    private Marker dropMarker;

    // Views
    private FloatingActionButton fabCurrentLocation;
    private Button locationBtn;
    private FloatingActionButton cleanBtn;
    private View line;

    private LocationManager mLocationManager;

    private PermissionListener mActivePermissionListener = new PermissionListener() {
        @Override
        public void onPermissionGranted(String... permissions) {
            mLocation = getLastKnownLocation();
            if (mGoogleMap != null && mLocation != null) {
                mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(mLocation.getLatitude(), mLocation.getLongitude()), 16));
            }
        }

        @Override
        public void onPermissionDenied(int... results) {

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        findView();
        init();
    }

    private void findView() {
        this.locationBtn = findViewById(R.id.locationBtn);
        this.fabCurrentLocation = findViewById(R.id.fabCurrentLocation);
        this.cleanBtn = findViewById(R.id.fabCleanBtn);
        this.line = findViewById(R.id.line);
    }

    private void init() {
        mLocationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

        if (!checkGPSService()) requestLocationService();
        else if (checkLocationPermissions()) requestLocationPermission();

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        locationBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mGoogleMap != null) {
                    if (pickUpLocation == null) {
                        pickUpLocation = mGoogleMap.getCameraPosition().target;
                        setPickUpMarker(pickUpLocation);

                        cleanBtn.setVisibility(View.VISIBLE);
                        locationBtn.setText("Set Drop");
                    } else {
                        dropLocation = mGoogleMap.getCameraPosition().target;
                        float distance = calculateDistance(pickUpLocation, dropLocation);
                        if (distance < 1000) {
                            Toast.makeText(HomeActivity.this, "The location you have selected is too close", Toast.LENGTH_SHORT).show();
                            dropLocation = null;
                            return;
                        }
                        setDropMarker(dropLocation);
                        addPolyline(pickUpLocation, dropLocation);

                        locationBtn.setVisibility(View.GONE);
                        line.setVisibility(View.GONE);
                        locationBtn.setText("Set Pickup");
                    }
                }
            }
        });

        cleanBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cleanBtn.setVisibility(View.GONE);
                locationBtn.setText("Set Pickup");
                locationBtn.setVisibility(View.VISIBLE);
                line.setVisibility(View.VISIBLE);
                pickUpLocation = null;
                dropLocation = null;
                if (pickupMarker != null) pickupMarker.remove();
                if (dropMarker != null) dropMarker.remove();
                if (mPolyLine != null) mPolyLine.remove();
            }
        });

        fabCurrentLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mLocation = getLastKnownLocation();
                if (mLocation != null) {
                    mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(mLocation.getLatitude(), mLocation.getLongitude()), 16));
                }
            }
        });

    }

    private void setPickUpMarker(LatLng latLng) {
        if (this.pickupMarker != null) {
            this.pickupMarker.remove();
        }
        pickupMarker = this.mGoogleMap.addMarker(
                new MarkerOptions()
                        .position(latLng)
                        .title("Pickup Location")
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_pin_pick_black_48dp)));
    }

    private void setDropMarker(LatLng latLng) {
        if (this.dropMarker != null) {
            this.dropMarker.remove();
        }
        this.dropMarker = this.mGoogleMap.addMarker(
                new MarkerOptions()
                        .position(latLng)
                        .title("Drop Location")
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_pin_drop_black_48dp)));
    }

    @SuppressLint("MissingPermission")
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap = googleMap;
        mGoogleMap.setMyLocationEnabled(true);
        mGoogleMap.getUiSettings().setMyLocationButtonEnabled(false);
        mLocation = getLastKnownLocation();
        if (mLocation != null) {
            mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(mLocation.getLatitude(), mLocation.getLongitude()), 16));
        }
    }

    private Location getLastKnownLocation() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return null;
        }

        mLocationManager = (LocationManager) getApplicationContext().getSystemService(LOCATION_SERVICE);
        List<String> providers = mLocationManager.getProviders(true);
        Location bestLocation = null;
        for (String provider : providers) {
            Location location = mLocationManager.getLastKnownLocation(provider);
            if (location == null) {
                continue;
            }

            // Selects the Best Location
            if (bestLocation == null || location.getAccuracy() < bestLocation.getAccuracy()) {
                bestLocation = location;
            }
        }
        return bestLocation;
    }

    private boolean checkGPSService() {
        return mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    private boolean checkLocationPermissions() {
        return ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED;
    }

    private void requestLocationPermission() {
        if (!checkLocationPermissions()) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION) ||
                    ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_COARSE_LOCATION)) {
                new AlertDialog.Builder(this)
                        .setTitle("Location Permission Needed")
                        .setMessage("This app needs the Location permission, please accept to use location service.")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                ActivityCompat.requestPermissions(HomeActivity.this, permissions, LOCATION_PERMISSIONS_REQUEST);
                            }
                        })
                        .setOnCancelListener(new DialogInterface.OnCancelListener() {
                            @Override
                            public void onCancel(DialogInterface dialog) {
                                mActivePermissionListener.onPermissionDenied();
                            }
                        })
                        .create()
                        .show();
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, LOCATION_PERMISSIONS_REQUEST);
            }
        } else {
            mActivePermissionListener.onPermissionGranted(permissions);
        }
    }

    private void requestLocationService() {
        if (!checkGPSService()) {
            new AlertDialog.Builder(this)
                    .setTitle("Location Service Needed")
                    .setMessage("This app needs the Location Service, please enable location service.")
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            Intent callGPSSettingIntent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                            startActivityForResult(callGPSSettingIntent, LOCATION_SERVICE_SETTING_REQUEST);
                        }
                    })
                    .create()
                    .show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case LOCATION_SERVICE_SETTING_REQUEST:
                if (checkGPSService()) return;
                if (checkLocationPermissions()) {
                    mActivePermissionListener.onPermissionGranted(permissions);
                } else {
                    requestLocationPermission();
                }
                break;
            default:
                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case LOCATION_PERMISSIONS_REQUEST:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mActivePermissionListener.onPermissionGranted(permissions);
                } else {
                    mActivePermissionListener.onPermissionDenied(grantResults);
                }
                break;
            default:
                Log.i("Permission Result", "Has not Implemented!!");
        }
    }

    private float calculateDistance(LatLng from, LatLng to) {
        float[] results = new float[1];
        Location.distanceBetween(from.latitude, from.longitude, to.latitude, to.longitude, results);
        return results[0];
    }

    private void addPolyline(final LatLng from, final LatLng to) {
        Toast.makeText(this, "Getting Path...", Toast.LENGTH_SHORT).show();
        new AsyncTask<String, Void, List<List<HashMap<String, String>>>>() {
            @Override
            protected List<List<HashMap<String, String>>> doInBackground(String... strings) {
                String data = "";
                InputStream istream = null;
                HttpURLConnection urlConnection = null;
                try {
                    URL url = new URL(getRestUrl(from, to));
                    urlConnection = (HttpURLConnection) url.openConnection();
                    urlConnection.connect();
                    istream = urlConnection.getInputStream();
                    BufferedReader br = new BufferedReader(new InputStreamReader(istream));
                    StringBuffer sb = new StringBuffer();
                    String line;
                    while ((line = br.readLine()) != null) {
                        sb.append(line);
                    }
                    data = sb.toString();
                    br.close();


                } catch (Exception e) {
                    Log.d("Reading url", e.toString());
                } finally {
                    try {
                        istream.close();
                    } catch (IOException | NullPointerException e) {
                        e.printStackTrace();
                    }
                    urlConnection.disconnect();
                }

                JSONObject jObject;
                List<List<HashMap<String, String>>> routes = null;
                try {
                    jObject = new JSONObject(data);
                    routes = parse(jObject);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return routes;
            }

            @Override
            protected void onPostExecute(List<List<HashMap<String, String>>> routes) {
                super.onPostExecute(routes);
                if (routes == null) {
                    Toast.makeText(HomeActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                    return;
                }
                ArrayList<LatLng> points = null;
                PolylineOptions polyLineOptions = null;

                for (int i = 0; i < routes.size(); i++) {
                    points = new ArrayList<LatLng>();
                    polyLineOptions = new PolylineOptions();
                    List<HashMap<String, String>> path = routes.get(i);

                    for (int j = 0; j < path.size(); j++) {
                        HashMap<String, String> point = path.get(j);

                        double lat = Double.parseDouble(point.get("lat"));
                        double lng = Double.parseDouble(point.get("lng"));
                        LatLng position = new LatLng(lat, lng);

                        points.add(position);
                    }

                    polyLineOptions.addAll(points);
                    polyLineOptions.width(4);
                    polyLineOptions.color(Color.BLUE);
                }

                mPolyLine = mGoogleMap.addPolyline(polyLineOptions);
            }
        }.execute();
    }

    private String getRestUrl(LatLng from, LatLng to) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("https://maps.googleapis.com/maps/api/directions/json");
        stringBuilder.append("?origin=");
        stringBuilder.append(from.latitude);
        stringBuilder.append(",");
        stringBuilder.append(from.longitude);
        stringBuilder.append("&destination=");
        stringBuilder.append(to.latitude);
        stringBuilder.append(",");
        stringBuilder.append(to.longitude);
        stringBuilder.append("&sensor=false&mode=driving&alternatives=true");
        stringBuilder.append(R.string.google_maps_key);
        return stringBuilder.toString();
    }

    public List<List<HashMap<String, String>>> parse(JSONObject jObject) {
        List<List<HashMap<String, String>>> routes = new ArrayList<>();
        JSONArray jRoutes = null;
        JSONArray jLegs = null;
        JSONArray jSteps = null;
        try {
            jRoutes = jObject.getJSONArray("routes");
            for (int i = 0; i < jRoutes.length(); i++) {
                jLegs = ((JSONObject) jRoutes.get(i)).getJSONArray("legs");
                List<HashMap<String, String>> path = new ArrayList<>();
                for (int j = 0; j < jLegs.length(); j++) {
                    jSteps = ((JSONObject) jLegs.get(j)).getJSONArray("steps");
                    for (int k = 0; k < jSteps.length(); k++) {
                        String polyline = "";
                        polyline = (String) ((JSONObject) ((JSONObject) jSteps.get(k)).get("polyline")).get("points");
                        List<LatLng> list = decodePoly(polyline);
                        for (int l = 0; l < list.size(); l++) {
                            HashMap<String, String> hm = new HashMap<>();
                            hm.put("lat", Double.toString(((LatLng) list.get(l)).latitude));
                            hm.put("lng", Double.toString(((LatLng) list.get(l)).longitude));
                            path.add(hm);
                        }
                    }
                    routes.add(path);
                }

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return routes;

    }

    private List<LatLng> decodePoly(String encoded) {
        List<LatLng> poly = new ArrayList<LatLng>();
        int index = 0, len = encoded.length();
        int lat = 0, lng = 0;

        while (index < len) {
            int b, shift = 0, result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lat += dlat;

            shift = 0;
            result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lng += dlng;

            LatLng p = new LatLng((((double) lat / 1E5)),
                    (((double) lng / 1E5)));
            poly.add(p);
        }
        return poly;
    }

    private interface PermissionListener {

        void onPermissionGranted(String... permissions);

        void onPermissionDenied(int... results);
    }
}
